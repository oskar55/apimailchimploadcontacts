<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');
ini_set('max_execution_time', 0);

// ministerio: 1 - hombres, 2 - mujeres, 3- jóvenes, 4 - jovenciatas
// Escuela hombres subgrupo 6009799096
$urlEscuMen = 'https://genesis.misionpaz.org/index.php/listasEmail/escuela/1/14/1';
// Escuela mujeres subgrupo  0ba933f44b
$urlEscuWomen = 'https://genesis.misionpaz.org/index.php/listasEmail/escuela/2/14/1';
// Escuela jovenes subgrupo 23f60abfc4
$urlEscuYong = 'https://genesis.misionpaz.org/index.php/listasEmail/escuela/3/14/1';
// Escuela jovencitas subgrupo 45d81ea674
$urlEscuGirl = 'https://genesis.misionpaz.org/index.php/listasEmail/escuela/4/14/1';
// Escuela niños subgrupo a4d724b8d4
$urlEscuKid = 'https://genesis.misionpaz.org/index.php/listasEmail/escuela/5/14/1';
// Escuela niñas subgrupo 18e7bc88fa
$urlEscuKidGirl = 'https://genesis.misionpaz.org/index.php/listasEmail/escuela/6/14/1';

for ($i=1; $i <= 7; $i++) {
  if ($i == 1) {
    $urlEscuela = $urlEscuMen;
    $idGroups = '6009799096';
  }
  if ($i == 2) {
    $urlEscuela = $urlEscuWomen;  
    $idGroups = '0ba933f44b'; 
  }
  if ($i == 3) {
    $urlEscuela = $urlEscuYong;  
    $idGroups = '23f60abfc4'; 
  }
  if ($i == 4) {
    $urlEscuela = $urlEscuGirl;  
    $idGroups = '45d81ea674'; 
  }
  if ($i == 5) {
    $urlEscuela = $urlEscuKid;  
    $idGroups = 'a4d724b8d4'; 
  }
  if ($i == 6) {
    $urlEscuela = $urlEscuKidGirl;  
    $idGroups = '18e7bc88fa'; 
  }

  //CONEXION CON GENESIS MPN
  ini_set("allow_url_fopen", 1);
  $json = file_get_contents($urlEscuela);
  $obj = json_decode($json, true);

  foreach($obj as $item){
    if ($item["mentor_ppal"] == "") {
      $item["mentor_ppal"] = "none";
    }
    if ($item["estudiante"] == "") {
      $item["estudiante"] = "none";
    }
    if ($item["ec"] == "") {
      $item["ec"] = "none";
    }
    if ($item["telefono"] == "") {
      $item["telefono"] = "none";
    }
    if ($item["celular"] == "") {
      $item["celular"] = "none";
    }
    if ($item["email"] == "") {
      $item["email"] = "none";
    }
    if ($item["estado_civil"] == "") {
      $item["estado_civil"] = "none";
    }
    if ($item["edad"] == "") {
      $item["edad"] = 0;
    }
    if ($item["fecha_nacimiento"] == "") {
      $item["fecha_nacimiento"] = "none";
    }

    if ($item["email"]) {
      $data = [
        'mentorpri' => $item["mentor_ppal"],
        'firstname' => $item["estudiante"],
        'estadoc'   => $item["estado_civil"],
        'telphone'  => $item["telefono"],
        'cellphone' => $item["celular"],
        'email'     => $item["email"],
        'edad'      => $item["edad"],
        'dob'       => $item["fecha_nacimiento"],
        'status'    => 'subscribed'
      ];
      //conexion y envio de datos a mailchimp
      syncMailchimp($data, $idGroups);
    }
  }

  }
  $i++;

  function syncMailchimp($data, $idGroups) {
      $apiKey = '4e8af884a6383c7264f2b60e25c58580-us17';
      $listId = '4b8a22178e';

      $memberId = md5(strtolower($data['email']));
      $dataCenter = substr($apiKey,strpos($apiKey,'-')+1);
      $url = 'https://' . $dataCenter . '.api.mailchimp.com/3.0/lists/' . $listId . '/members/' . $memberId;

      $json = json_encode([
          'email_address' => $data['email'],
          'status'        => $data['status'], // "subscribed","unsubscribed","cleaned","pending"
          'merge_fields'  => [
              'FNAME'     => $data['firstname'],
              'PHONE'     => $data['telphone'],
              'MTHELCELL' => $data['cellphone'],
              'MMENTORPRI'=> $data['mentorpri'],
              'MECIVIL'   => $data['estadoc'],
              'MEDAD'     => $data['edad'],
              'MDATEOB'   => $data['dob']
          ],
          'interests'     => [
              $idGroups  => true
          ]
      ]);
      $ch = curl_init($url);
      curl_setopt($ch, CURLOPT_USERPWD, 'user:' . $apiKey);
      curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_TIMEOUT, 10);
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $json);

      $result = curl_exec($ch);
      $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    
      curl_close($ch);
      return $httpCode;

      // echo $result."<br>";
      // echo $httpCode."<br>";
  }




?>