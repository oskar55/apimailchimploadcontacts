<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');
ini_set('max_execution_time', 0);

// ministerio: 1 - hombres, 2 - mujeres, 3- jóvenes, 4 - jovenciatas

// Grupos de Paz Hombres subgrupo  55b0bc1386
$urlGroPmen = 'https://genesis.misionpaz.org/index.php/listasEmail/grupos-paz/1/10/1';
// Grupos de Paz Mujeres subgrupo  47bd91583c
$urlGroPwomen = 'https://genesis.misionpaz.org/index.php/listasEmail/grupos-paz/2/10/1';
// Grupos de Paz Jovenes subgrupo  7466cafafe
$urlGroPyong = 'https://genesis.misionpaz.org/index.php/listasEmail/grupos-paz/3/10/1';
// Grupos de Paz Jovencitas subgrupo  5a0e17a75a
$urlGroPgirl = 'https://genesis.misionpaz.org/index.php/listasEmail/grupos-paz/4/10/1';
// Grupos de Paz Niños subgrupo  2e1fa0d227
$urlGroPkid = 'https://genesis.misionpaz.org/index.php/listasEmail/grupos-paz/5/10/1';
// Grupos de Paz Niñas subgrupo  c54abf65a2
$urlGroPkidGirl = 'https://genesis.misionpaz.org/index.php/listasEmail/grupos-paz/6/10/1';



for ($i=1; $i <= 7; $i++) {
  if ($i == 1) {
    $urlGrupo = $urlGroPmen;
    $idGroups = '55b0bc1386';
  }
  if ($i == 2) {
    $urlGrupo = $urlGroPwomen;  
    $idGroups = '47bd91583c'; 
  }
  if ($i == 3) {
    $urlGrupo = $urlGroPyong;  
    $idGroups = '7466cafafe'; 
  }
  if ($i == 4) {
    $urlGrupo = $urlGroPgirl;  
    $idGroups = '5a0e17a75a'; 
  }
  if ($i == 5) {
    $urlGrupo = $urlGroPkid;  
    $idGroups = '2e1fa0d227'; 
  }
  if ($i == 6) {
    $urlGrupo = $urlGroPkidGirl;  
    $idGroups = 'c54abf65a2'; 
  }


//CONEXION CON GENESIS MPN
ini_set("allow_url_fopen", 1);
$json = file_get_contents($urlGrupo);
$obj = json_decode($json, true);

foreach($obj as $item){

  if ($item["mentor_ppal"] == "") {
    $item["mentor_ppal"] = "none";
  }
  if ($item["nombre_mentor"] == "") {
    $item["nombre_mentor"] = "none";
  }
  if ($item["telefono"] == "") {
    $item["telefono"] = "none";
  }
  if ($item["profesion"] == "") {
    $item["profesion"] = "none";
  }
  if ($item["comuna"] == "") {
    $item["comuna"] = "none";
  }
  if ($item["barrio"] == "") {
    $item["barrio"] = "none";
  }
  if ($item["estado_civil"] == "") {
    $item["estado_civil"] = "none";
  }
  if ($item["edad"] == "") {
    $item["edad"] = 0;
  }
  if ($item["fecha_nacimiento"] == "") {
    $item["fecha_nacimiento"] = 0;
  }

  if ($item["correo_mentor"]) {
    $data = [
      'mentorPpal' => $item["mentor_ppal"],
      'firstname' => $item["nombre_mentor"],
      'email'     => $item["correo_mentor"],
      'telphone'  => $item["telefono"],
      'profesion' => $item["profesion"],
      'comuna'    => $item["comuna"],
      'barrio'    => $item["barrio"],
      'estadoc'   => $item["estado_civil"],
      'edad'       => $item["edad"],
      'DOB'       => $item["fecha_nacimiento"],
      'status'    => 'subscribed'
    ];
    //conexion y envio de datos a mailchimp
    syncMailchimp($data, $idGroups);
  }
}

}
$i++;

function syncMailchimp($data, $idGroups) {
    $apiKey = '4e8af884a6383c7264f2b60e25c58580-us17';
    $listId = '4b8a22178e';

    $memberId = md5(strtolower($data['email']));
    $dataCenter = substr($apiKey,strpos($apiKey,'-')+1);
    $url = 'https://' . $dataCenter . '.api.mailchimp.com/3.0/lists/' . $listId . '/members/' . $memberId;

    $json = json_encode([
        'email_address' => $data['email'],
        'status'        => $data['status'], // "subscribed","unsubscribed","cleaned","pending"
        'merge_fields'  => [
            'FNAME'     => $data['firstname'],
            'BARRIO'     => $data['barrio'],
            'PHONE'     => $data['telphone'],
            'MMENTORPRI'   => $data['mentorPpal'],
            'MECIVIL'   => $data['estadoc'],
            'MEDAD'  => $data['edad'],
            'MDATEOB'  => $data['DOB'],
            'MCOMUNA' => $data['comuna'],
            'MPROFESION' => $data['profesion']
        ],
        'interests'     => [
            $idGroups  => true
        ]
    ]);

    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_USERPWD, 'user:' . $apiKey);
    curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_TIMEOUT, 10);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $json);

    $result = curl_exec($ch);
    $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

    // echo $result;
    // echo "<br><br><br>";

    curl_close($ch);
    return $httpCode;

}

?>
